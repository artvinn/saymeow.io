import React, { Component } from 'react'
import styled from 'styled-components'

import GamePageHelmet from '../components/helmets/game-page-helmet'
import GameIFrame from '../components/game-iframe'

export default ({ data }) => {
  const game = data.games.edges[0].node
  return (
    <div>
      <GamePageHelmet gameData={game} siteMetadata={data.site.siteMetadata} />
      <GameIFrame game={game} isMobile={true} />
    </div>
  )
}

// TODO: duplication of game.js query. Move to fragment?
export const query = graphql`
  query GamePageMobileQuery($key: String!) {
    games: allGamesJson(filter: { key: { eq: $key } }) {
      edges {
        node {
          name
          description
          metaDescription
          key
          width
          height
          selfHosted
          link
          tags
          previewImageAltText
        }
      }
    }

    site {
      siteMetadata {
        siteUrl
        previewImageSize
      }
    }
  }
`

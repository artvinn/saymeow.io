import React from 'react'
import { Helmet } from 'react-helmet'

import GamesList from '../components/games-list'

function wordsToLowerCase(str) {
  return str.replace(/\b\w/g, (w) => w.toLowerCase())
}

export default ({ data, pathContext }) => {
  const tagName = wordsToLowerCase(pathContext.tag)
  return (
    <div>
      <Helmet>
        <title>{`Play ${tagName} games for free at saymeow.io.`}</title>
        <meta httpEquiv="Content-Language" content="en" />
        <meta
          name="description"
          content={`At saymeow.io you can play best ${tagName} games online, absolutely free of charge. We selected the most interesting and fun ${tagName} games specially for you, so come check them out!`}
        />
      </Helmet>
      <GamesList
        sectionName={`${pathContext.tag} Games`}
        games={data.games.edges.map((edge) => edge.node)}
      />
    </div>
  )
}

export const pageQuery = graphql`
  query TagPage($tag: String) {
    games: allGamesJson(
      filter: { tags: { in: [$tag] } }
      sort: { fields: [createdAt], order: DESC }
    ) {
      edges {
        node {
          name
          key
        }
      }
    }
  }
`

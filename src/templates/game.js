import React from 'react'
import styled from 'styled-components'
import isMobile from 'ismobilejs'

import GamePageHelmet from '../components/helmets/game-page-helmet'
import GameBox from '../components/game-box'
import GameInfo from '../components/game-info'
import RecommendedGamesList from '../components/recommended-games-list'
import GamepadImage from '../gamepad.png'

const Wrapper = styled.section`
  display: grid;
  grid-column: 1 / -1;
  grid-template-columns: 1fr minmax(auto, 960px) 1fr;
  margin-top: 20px;
`

const PlayButton = styled.a`
  margin: 0 auto;
  width: 170px;
  height: 60px;
  border-radius: 10px;
  background: #09c559;
  color: #ffffff;
  text-decoration: none;
  outline: none;
  border: none;
`

const Gamepad = styled.img`
  margin: 0;
  top: 10px;
  position: relative;
  margin-left: 15px;
`

const PlayText = styled.span`
  font-size: 2.1em;
  margin-left: 10px;
`

const Ad1 = styled.div`
  display: grid;
`

const Ad2 = styled.div`
  display: grid;
  grid-column: 3 / -1;
  grid-row: 1;
`

const PostScriptumWrapper = styled.article`
  background-color: #ffffff;
  box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2),
    0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12);
  padding: 15px;
  max-width: 840px;
  margin: 0 auto;
  text-align: center;
  margin-top: 0px;
  margin-bottom: 25px;
  grid-row: 3;
  grid-column: 2;
`

const PostScriptum = ({ tags, name }) => {
  const [link1, link2] = tags.map((tag) => {
    tag = tag.toLowerCase()
    return <a href={`/tags/${tag}`}>{tag}</a>
  })

  return (
    <PostScriptumWrapper>
      If you enjoyed playing <b>{name}</b> you can play other {link1}{' '}
      {link2 ? <span>or {link2}</span> : ''} HTML5 games on our website,
      absolutely for free, just don't forget to say meow first ;) Have fun!
    </PostScriptumWrapper>
  )
}

export default ({ data }) => {
  const gameData = data.games.edges[0].node
  const pageTitle = `${
    gameData.name
  } - Play ${gameData.tags[0].toLowerCase()} games at saymeow.io`

  return (
    <Wrapper>
      <GamePageHelmet
        gameData={gameData}
        siteMetadata={data.site.siteMetadata}
      />
      <Ad1 />

      {isMobile.any ? (
        <PlayButton href={`${window.location.href}/play`}>
          <Gamepad src={GamepadImage} />
          <PlayText>Play</PlayText>
        </PlayButton>
      ) : (
        <GameBox gameData={gameData} />
      )}

      <GameInfo tags={gameData.tags} gameData={gameData} />
      <PostScriptum tags={gameData.tags} name={gameData.name} />

      <Ad2 />
    </Wrapper>
  )
}

export const query = graphql`
  query GamePageQuery($key: String!) {
    games: allGamesJson(filter: { key: { eq: $key } }) {
      edges {
        node {
          name
          description
          metaDescription
          key
          width
          height
          selfHosted
          link
          tags
          previewImageAltText
        }
      }
    }

    site {
      siteMetadata {
        siteUrl
        previewImageSize
      }
    }
  }
`

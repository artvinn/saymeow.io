import React from 'react'
import Link from 'gatsby-link'
import uuidv1 from 'uuid/v1'
import { Helmet } from 'react-helmet'

const TagsPage = ({
  data: {
    allTags: { edges },
  },
}) => (
  <div>
    <div>
      <h1>Tags</h1>
      <ul>
        {edges.map(({ node }) => (
          <li key={uuidv1()}>
            <Link to={`/tags/${node.key}`}>
              {node.name}({node.numOfGames})
            </Link>
          </li>
        ))}
      </ul>
    </div>
  </div>
)

export default TagsPage

export const query = graphql`
  query TagsQuery {
    allTags {
      edges {
        node {
          name
          key
          numOfGames
        }
      }
    }
  }
`

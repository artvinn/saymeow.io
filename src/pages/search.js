import React, { Component } from 'react'
import { Index } from 'elasticlunr'

import TagsList from '../components/tags-list'
import GamesList from '../components/games-list'
import getUrlParameter from '../utils/getUrlParameter'

class SearchResult extends Component {
  constructor(props) {
    super()

    this.query = getUrlParameter(props.location, 'query')
    this.siteSearchIndex = props.data.siteSearchIndex

    this.state = {
      results: { games: [], tags: [] },
    }
  }

  componentWillReceiveProps(props) {
    this.query = getUrlParameter(props.location, 'query')
    this.siteSearchIndex = props.data.siteSearchIndex
    this.search()
  }

  getOrCreateIndex = () =>
    this.index ? this.index : Index.load(this.siteSearchIndex.index)

  componentWillMount() {
    this.index = this.getOrCreateIndex()
    this.search()
  }

  search() {
    const results = this.index
      .search(this.query, { expand: true })
      .map(({ ref }) => this.index.documentStore.getDoc(ref))

    const games = results
      .filter((el) => el.id.includes('game-'))
      .sort((a, b) => {
        if (a.createdAt > b.createdAt) return -1
        if (a.createdAt < b.createdAt) return 1
        return 0
      })

    const tags = results.filter((el) => el.id.includes('tag-'))

    this.setState({
      results: { games, tags },
    })
  }

  render() {
    const hasTags = this.state.results.tags.length > 0
    const hasGames = this.state.results.games.length > 0

    return (
      <div>
        {hasTags ? (
          <TagsList sectionName={`Tags`} tags={this.state.results.tags} />
        ) : null}
        {hasGames ? (
          <GamesList sectionName={`Games`} games={this.state.results.games} />
        ) : null}
      </div>
    )
  }
}

export default SearchResult

export const query = graphql`
  query SearchResultQuery {
    siteSearchIndex {
      index
    }
  }
`

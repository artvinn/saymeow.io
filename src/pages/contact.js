import React from 'react'

export default () => (
  <div>
    <h1 style={{ textAlign: 'center' }}>Contact us</h1>
    <p>For any inquiries you can reach out to us at info@saymeow.io</p>
  </div>
)

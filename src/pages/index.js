import React from 'react'

import GamesList from '../components/games-list'

export default ({ data }) => {
  return (
    <GamesList
      sectionName={'New Games'}
      games={data.games.edges.map((edge) => edge.node)}
    />
  )
}

export const query = graphql`
  query IndexQuery {
    games: allGamesJson(sort: { fields: [createdAt], order: DESC }) {
      edges {
        node {
          name
          key
          previewImageAltText
        }
      }
    }
  }
`

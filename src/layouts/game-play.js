import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  overflow: hidden;
`

export default ({ children }) => <Wrapper>{children()}</Wrapper>

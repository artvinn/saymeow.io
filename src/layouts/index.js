import React from 'react'
import styled from 'styled-components'
import { Helmet } from 'react-helmet'

import '../utils/media-queries'
import '../utils/global-styles'
import Header from './header'
import Footer from './footer'
import NavMenu from '../components/nav-menu'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
`

const Main = styled.div`
  display: flex;
  justify-content: center;
  min-height: 100vh;
  margin: 15px 15px;
`

export default ({ children, data, location }) => (
  <Wrapper>
    <Helmet>
      <title>Play free games online at saymeow.io!</title>
      <meta httpEquiv="Content-Language" content="en" />
      <meta
        name="description"
        content={`Say meow and play best games online, absolutely free of charge. We selected the most interesting and fun games specially for you, so come check them out!`}
      />
      <link rel="alternate" href="https://saymeow.io" hreflang="en-us" />
    </Helmet>
    <Header
      title={data.site.siteMetadata.title}
      siteSearchIndex={data.siteSearchIndex}
    />
    <NavMenu location={location} />
    <Main>{children()}</Main>
    <Footer />
  </Wrapper>
)

export const query = graphql`
  query LayoutQuery {
    site {
      siteMetadata {
        title
      }
    }

    siteSearchIndex {
      index
    }
  }
`

import React from 'react'
import styled from 'styled-components'
import Link from 'gatsby-link'

const Footer = styled.footer`
  background-color: #005eb2;
  color: #ffffff;
  font-size: 14px;
  text-align: center;
`

const Copyright = styled.p`
  font-size: 0.8em;
`

const Links = styled.ul`
  list-style-type: none;
  padding: 0;

  li {
    display: inline-block;

    &:not(:last-child):after {
      content: ' |';
      padding: 0px 10px;
    }
  }

  a {
    text-decoration: none;
    color: #ffffff;
  }
`

export default () => {
  const year = new Date().getUTCFullYear().toString()

  return (
    <Footer>
      <div>
        <Copyright
        >{`Copyright © ${year} saymeow.io All rights reserved.`}</Copyright>

        <Links>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/privacy-policy">Privacy Policy</Link>
          </li>

          <li>
            <Link to="/contact">Contact</Link>
          </li>
        </Links>
      </div>
    </Footer>
  )
}

import React from 'react'
import styled from 'styled-components'
import Link from 'gatsby-link'

import websiteLogo from '../website-logo.png'
import Search from '../components/search'

const Wrapper = styled.div`
  display: flex;
  height: 55px;
  justify-content: space-between;
  align-items: center;
  background-color: #005fb2;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  z-index: 4;
`

const StyledLink = styled(Link)`
  display: inline-block;
  text-decoration: none;
`

const Logo = styled.img`
  width: 120px;
  position: relative;
  margin-left: 20px;
  margin-top: 25px;
`

const Header = ({ title, siteSearchIndex }) => (
  <Wrapper>
    <StyledLink to="/">
      <Logo
        src={websiteLogo}
        alt={'Saymeow.io - play best free games online!'}
      />
    </StyledLink>
    <Search siteSearchIndex={siteSearchIndex} />
  </Wrapper>
)

export default Header

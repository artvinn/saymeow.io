import React from 'react'
import styled from 'styled-components'
import uuidv1 from 'uuid/v1'
import Link from 'gatsby-link'

import kebabCase from '../utils/kebabCase'
import media from '../utils/media-queries'
import Button from './styles/button'

const Wrapper = styled.article`
  display: grid;
  grid-template-columns: 1fr;
  grid-auto-rows: min-content min-content;
  grid-column: 2;
  position: relative;
  box-shadow: 1px 1px 6px 0px rgba(0, 0, 0, 0.3);
  margin-top: 15px;
  margin-right: 10px;
  margin-bottom: 30px;
  margin-left: 10px;
  grid-column-gap: 10px;
  background-color: #ffffff;

  & > *:not(img) {
    margin-left: 15px;
  }
`

const Title = styled.h2`
  font-size: 1.4em;
  color: #000000;
  margin: 15px 0px 15px 0px;

  ${media.mobileSmall`font-size: 1.4em`};
`

const TagsWrapper = styled.div`
  margin: -10px 0 0 0;

  & > * {
    margin-top: 10px;
  }
`

const Image = styled.img`
  position: relative;
  float: right;
  max-width: 180px;
  padding-left: 5px;
  padding-bottom: 5px;

  ${media.mobileSmall`max-width: 140px`};
`

const Main = styled.div`
  margin-top: 15px;
  margin-right: 20px;
  grid-column: 1;
`

const Description = styled.div`
  & > p {
    margin-top: 0;
  }

  ul {
    padding-left: 20px;
  }

  ul li {
    padding-bottom: 8px;
  }
`

const StyledLink = styled(Link)`
  ${Button} margin-right: 15px;
  font-size: 12px;
  font-weight: 400;
  padding: 6px 14px;
`

const Breadcrumb = styled.ol`
  display: inline-block;
  padding: 0;
  margin: 0;
  margin-bottom: 10px;

  li {
    display: inline-block;
    padding-right: 5px;
  }

  li:not(:first-child) {
    padding-left: 5px;
  }

  a {
    text-decoration: none;
  }
`
const GameInfo = ({ tags, gameData }) => {
  return (
    <Wrapper>
      <header>
        <Title>Game Details</Title>
        <TagsWrapper>
          {tags.map((tag) => (
            <StyledLink key={uuidv1()} to={`/tags/${kebabCase(tag)}`}>
              {tag.toUpperCase()}
            </StyledLink>
          ))}
        </TagsWrapper>
      </header>

      <Main>
        <Image
          src={`/images/games/${gameData.key}.jpg`}
          alt={gameData.previewImageAltText}
          title={gameData.name}
        />
        <Description
          dangerouslySetInnerHTML={{ __html: gameData.description }}
        />

        <h4 style={{ marginBottom: 5 }}>Category</h4>
        <Breadcrumb>
          <li>
            <Link to="/tags">Tags</Link>
          </li>
          ›
          <li>
            <Link to={`/tags/${kebabCase(tags[0])}`}>{tags[0]}</Link>
          </li>
          ›
          <li>
            <Link to={`/games/${gameData.key}`}>{gameData.name}</Link>
          </li>
        </Breadcrumb>
      </Main>
    </Wrapper>
  )
}

export default GameInfo

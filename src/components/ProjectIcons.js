import React from 'react'
import { Icon } from 'react-icons-kit'
import { enlarge } from 'react-icons-kit/icomoon/enlarge'
import { share2 } from 'react-icons-kit/icomoon/share2'
import { cross } from 'react-icons-kit/icomoon/cross'
import { search } from 'react-icons-kit/icomoon/search'

export const ExpandIcon = (props) => (
  <Icon size={20} icon={enlarge} {...props} />
)
export const ShareIcon = (props) => <Icon size={24} icon={share2} {...props} />
export const CrossIcon = (props) => <Icon icon={cross} {...props} />
export const SearchIcon = (props) => <Icon icon={search} {...props} />

import React from 'react'
import { Helmet } from 'react-helmet'
import kebabCase from '../../utils/kebabCase'

const BreadcrumbData = (gameData, siteMetadata) => {
  const mainTag = gameData.tags[0]
  const tagsUrl = `${siteMetadata.siteUrl}/tags`
  const tagUrl = `${tagsUrl}/${kebabCase(mainTag)}`
  const gameUrl = `${siteMetadata.siteUrl}/games/${gameData.key}`

  return {
    '@context': 'http://schema.org',
    '@type': 'BreadcrumbList',
    itemListElement: [
      {
        '@type': 'ListItem',
        position: 1,
        item: {
          '@id': `${tagsUrl}`,
          name: 'Tags',
        },
      },
      {
        '@type': 'ListItem',
        position: 2,
        item: {
          '@id': `${tagUrl}`,
          name: `${mainTag}`,
        },
      },
      {
        '@type': 'ListItem',
        position: 3,
        item: {
          '@id': `${gameUrl}`,
          name: `${gameData.name}`,
        },
      },
    ],
  }
}

export default ({ gameData, siteMetadata }) => {
  const pageTitle = `${gameData.name} - Play ${gameData.name} at saymeow.io`

  const gameUrl = `${siteMetadata.siteUrl}/games/${gameData.key}`
  const imageUrl = `${siteMetadata.siteUrl}/images/games/${gameData.key}.jpg`

  return (
    <Helmet>
      {/* General tags */}
      <title>{pageTitle}</title>
      <meta httpEquiv="Content-Language" content="en" />
      <meta name="description" content={gameData.metaDescription} />

      {/* schema.org tags */}
      <script type="application/ld+json">
        {JSON.stringify(BreadcrumbData(gameData, siteMetadata))}
      </script>

      {/* Open Graph tags */}
      <meta property="og:site_name" content="Saymeow Games" />
      <meta property="og:title" content={gameData.name} />
      <meta property="og:type" content="website" />
      <meta property="og:url" content={gameUrl} />
      <meta property="og:description" content={gameData.metaDescription} />
      <meta property="og:image" content={imageUrl} />
      <meta property="og:image:alt" content={gameData.previewImageAltText} />
      <meta property="og:image:width" content={siteMetadata.previewImageSize} />
      <meta
        property="og:image:height"
        content={siteMetadata.previewImageSize}
      />

      {/* Twitter Card tags */}
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@saymeow" />
      <meta name="twitter:title" content={gameData.name} />
      <meta name="twitter:description" content={gameData.metaDescription} />
      <meta name="twitter:image" content={imageUrl} />
    </Helmet>
  )
}

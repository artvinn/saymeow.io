import React from 'react'
import styled from 'styled-components'
import uuidv1 from 'uuid/v1'
import PropTypes from 'prop-types'

import media from '../utils/media-queries'
import GameCard from './game-card'
import withSectionTitle from './withSectionTitle'

const Wrapper = styled.div`
  display: grid;
  grid-gap: 20px;
  grid-auto-rows: min-content;
  max-width: 1280px;
  margin-top: 30px;
  font-size: 16px;

  ${media.desktop`grid-template-columns: repeat(6, 1fr);`};
  ${media.laptop`grid-template-columns: repeat(5, 1fr);`};
  ${media.tablet`grid-template-columns: repeat(4, 1fr); font-size: 14px;`};
  ${media.mobile`grid-template-columns: repeat(3, 1fr);`};
  ${media.mobileSmall`grid-template-columns: repeat(2, 1fr);`};
`

const GamesList = ({ games }) => (
  <Wrapper>
    {games.map((game) => {
      const { key, name, previewImageAltText } = game
      return (
        <GameCard
          key={uuidv1()}
          gameKey={key}
          gameName={name}
          previewImageAltText={previewImageAltText}
          mini={false}
        />
      )
    })}
  </Wrapper>
)

GamesList.propTypes = {
  games: PropTypes.arrayOf(PropTypes.object),
}

export default withSectionTitle(GamesList)

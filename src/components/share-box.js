import React, { Component } from 'react'
import styled from 'styled-components'

import ButtonStyle from './styles/button'
import ShareOnSocial from './share-on-social'
import { CrossIcon } from './ProjectIcons'

const Wrapper = styled.div`
  display flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 350px;
  background-color: #ffffff;
  z-index: 999;
  padding: 20px 15px;
`

const Header = styled.div`
  display: inline-flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;

  & * {
    margin: 0;
  }
`

const Body = styled.div`
  display: flex;
  width: 100%;
  margin-top: 25px;
  align-items: center;
`

const Input = styled.input`
  width: 100%;
  padding: 10px 70px 10px 5px;
  border: none;
  outline: 0;
  border: 1px solid #e8e8e8;
  background-color: #e6f3ff;
  border-radius: 2px;
`

const CopyButton = styled.button`
  ${ButtonStyle};

  position: absolute;
  right: 25px;
  padding: 4px 10px;
`

class ShareBox extends Component {
  constructor() {
    super()

    this.state = {
      inputSelected: false,
      link: `${window.location.href}`,
    }
  }

  handleInputFocus = (event) => {
    event.target.select()
  }

  handleCopyClick = () => {
    if (this.linkInput !== null) {
      this.linkInput.select()
      document.execCommand('copy')
    }
  }

  render() {
    const { name, metaDescription } = this.props.gameData

    return (
      <Wrapper>
        <Header>
          <h4>Share {name}</h4>
          <div style={{ cursor: 'pointer' }} onClick={this.props.onClose}>
            <CrossIcon size={16} />
          </div>
        </Header>

        <ShareOnSocial title={`Play ${name}`} description={metaDescription} />

        <Body>
          <Input
            readOnly
            type="text"
            onFocus={this.handleInputFocus}
            value={this.state.link}
            innerRef={(ref) => (this.linkInput = ref)}
          />
          <CopyButton onClick={this.handleCopyClick}>Copy</CopyButton>
        </Body>
      </Wrapper>
    )
  }
}

export default ShareBox

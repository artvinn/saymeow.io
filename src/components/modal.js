import React, { Component } from 'react'
import styled from 'styled-components'
import onClickOutside from 'react-onclickoutside'

const Overlay = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: rgba(0.5, 0.5, 0.5, 0.6);
  z-index: 998;
`

const Modal = onClickOutside(
  class extends Component {
    handleClickOutside = (event) => {
      this.props.hideOverlay()
    }

    render() {
      return (
        <div
          style={{
            position: 'fixed',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
          }}
        >
          {this.props.children}
        </div>
      )
    }
  }
)

export default class extends Component {
  hideOverlay = () => {
    if (this.props.onClose) {
      this.props.onClose()
    }
  }

  render() {
    return (
      <Overlay>
        <Modal hideOverlay={this.hideOverlay}>{this.props.children}</Modal>
      </Overlay>
    )
  }
}

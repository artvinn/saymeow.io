import React, { Component } from 'react'
import { Index } from 'elasticlunr'
import uuidv1 from 'uuid/v1'
import styled from 'styled-components'
import { navigateTo } from 'gatsby-link'
import onClickOutside from 'react-onclickoutside'

import { SearchIcon } from './ProjectIcons'

const Wrapper = styled.div`
  padding: 0;
  margin: 0;
  margin-right: 20px;
  z-index: 2;
  display: inline-block;
`

const InputWrapper = styled.div`
  display: flex;
  position: relative;
  width: 200px;
`

const Input = styled.input`
  position: relative;
  padding-left: 0.1em;
  padding-right: 2.5em;
  width: 180px;
  height: 30px;
  border-radius: 8px;
  text-indent: 10px;
  border: none;
  outline: none;
`

const SearchIconWrapper = styled.div`
  position: absolute;
  top: 5px;
  right: 10px;
  display: inline-block;
  cursor: pointer;
  color: #2192ed;
`

const List = styled.ul`
  position: absolute;
  padding: 0;
  width: 200px;
  margin-top: 5px;
`

const ListElement = styled.li`
  list-style-type: none;
  background-color: #ffffff;
  border-radius: 4px;
  border-bottom: 2px solid #005fb2;
  padding: 10px;

  a {
    color: #29b2e2;
    text-decoraion: none;
  }

  &:hover {
    cursor: pointer;
    background-color: #29b2e2;

    a {
      color: #ffffff;
    }
  }
`

class Search extends Component {
  constructor(props) {
    super(props)

    this.index = null
    this.state = {
      query: '',
      results: { games: [], tags: [] },
      expand: false,
    }
  }

  resetState() {
    this.setState({ expand: false, query: '' })
  }

  handleClickOutside = (event) => {
    if (this.state.expand) {
      this.resetState()
    }
  }

  handleLinkClick = (type, key) => {
    this.navigateTo(`/${type}/${key}`)
  }

  handleViewAllClick = () => {
    this.navigateTo(`/search?query=${this.state.query}`)
  }

  navigateTo(path) {
    if (this.state.query.length > 0) {
      this.resetState()
      navigateTo(path)
    }
  }

  handleKeyDown = (event) => {
    if (event.keyCode === 13) {
      event.preventDefault()
      this.handleViewAllClick()
    }
  }

  getOrCreateIndex = () =>
    this.index ? this.index : Index.load(this.props.siteSearchIndex.index)

  search = (event) => {
    const query = event.target.value

    if (query.length < 2) {
      this.setState({ query, expand: false })
      return
    }

    this.index = this.getOrCreateIndex()
    const results = this.index
      .search(query, { expand: true })
      .map(({ ref }) => this.index.documentStore.getDoc(ref))

    const games = results.filter((el) => el.id.includes('game-'))
    const tags = results.filter((el) => el.id.includes('tag-'))

    this.setState({
      query,
      results: { games, tags },
      expand: [...games, ...tags].length > 0,
    })
  }

  render() {
    return (
      <Wrapper>
        <InputWrapper>
          <Input
            placeholder={'Search:'}
            type="text"
            value={this.state.query}
            onChange={this.search}
            onKeyDown={this.handleKeyDown}
          />
          <SearchIconWrapper onClick={this.handleViewAllClick}>
            <SearchIcon size={18} />
          </SearchIconWrapper>
        </InputWrapper>

        <List style={{ display: this.state.expand ? 'block' : 'none' }}>
          {this.state.results.tags.map(({ name, key }) => (
            //render tags first
            <ListElement
              key={uuidv1()}
              onClick={() => this.handleLinkClick('tags', key)}
            >
              {`# ${name} Games`}
            </ListElement>
          ))}

          {this.state.results.games.map(({ name, key }) => (
            //render games
            <ListElement
              key={uuidv1()}
              onClick={() => this.handleLinkClick('games', key)}
            >
              {name}
            </ListElement>
          ))}

          <ListElement key={uuidv1()} onClick={this.handleViewAllClick}>
            View all results
          </ListElement>
        </List>
      </Wrapper>
    )
  }
}

export default onClickOutside(Search)

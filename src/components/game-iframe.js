import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledIFrame = styled.iframe`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border: none;
`

const iFrameProps = {
  scrolling: 'no',
}

const getGameUrl = ({ key, selfHosted, link }) => {
  const url = 'https://s3.us-east-2.amazonaws.com/saymeow.io/games'
  return selfHosted ? `${url}/${key}/index.html` : link
}

const DesktopIFrame = ({ game }) => (
  <StyledIFrame src={getGameUrl(game)} {...iFrameProps} />
)

class MobileIFrame extends Component {
  constructor() {
    super()

    this.state = {
      windowWidth: undefined,
      windowHeight: undefined,
    }
  }

  handleResize() {
    this.setState({
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
    })
  }

  componentDidMount() {
    this.handleResize()
    window.addEventListener('resize', () => this.handleResize())
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => this.handleResize())
  }

  render() {
    const { windowWidth, windowHeight } = this.state
    const game = this.props.game
    // const w = game.width
    // const h = game.height
    const w = 1444
    const h = 720
    const scaleX = windowWidth / w
    const scaleY = windowHeight / h
    const scale = Math.min(scaleX, scaleY)
    const gameWidth = w * scaleX
    const gameHeight = h * scaleY

    return (
      <StyledIFrame
        width={gameWidth}
        height={gameHeight}
        src={getGameUrl(game)}
        {...iFrameProps}
      />
    )
  }
}

export default ({ game, isMobile }) =>
  isMobile ? <MobileIFrame game={game} /> : <DesktopIFrame game={game} />

MobileIFrame.propTypes = DesktopIFrame.propTypes = {
  game: PropTypes.shape({
    width: PropTypes.number,
    height: PropTypes.number,
    key: PropTypes.string,
    link: PropTypes.string,
    selfHosted: PropTypes.bool,
  }),
}

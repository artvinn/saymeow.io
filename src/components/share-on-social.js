import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import FacebookImage from '../images/social/facebook.svg'
import TwitterImage from '../images/social/twitter.svg'
import TumblrImage from '../images/social/tumblr.svg'
import TelegramImage from '../images/social/telegram.svg'
import GooglePlusImage from '../images/social/google-plus.svg'
import VkImage from '../images/social/vk.svg'

const HASHTAGS = 'html5games, play, saymeow'

const SocialsWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  margin-top: 25px;

  img {
    width: 50px;
    height: 50px;
  }
`

const Icon = styled.img`
  cursor: pointer;
`

class ShareOnSocial extends Component {
  handleFacebookClick = () => {
    window.open(
      `https://www.facebook.com/sharer/sharer.php?u=${window.location.href}`
    )
  }

  handleTwitterClick = () => {
    const text = this.props.title
    const link = `https://twitter.com/intent/tweet?url=${
      window.location.href
    }&text=${text}&hashtags=${HASHTAGS}`

    window.open(link)
  }

  handleTumblrClick = () => {
    const link = `https://www.tumblr.com/widgets/share/tool?canonicalUrl=${
      window.location.href
    }&title=${this.props.title}&content=${
      window.location.href
    }&tags=${HASHTAGS}&caption=${this.props.description}`

    window.open(link)
  }

  handleTelegramClick = () => {
    const link = `https://telegram.me/share/url?url=${
      window.location.href
    }&text=${this.props.title}`

    window.open(link)
  }

  handleGooglePlusClick = () => {
    const link = `https://plus.google.com/share?url=${
      window.location.href
    }&text=${this.props.description}`

    window.open(link)
  }

  handleVkClick = () => {
    const link = `http://vk.com/share.php?url=${window.location.href}&title=${
      this.props.title
    }&comment=${this.props.description}`

    window.open(link)
  }

  render() {
    return (
      <SocialsWrapper>
        <Icon
          src={FacebookImage}
          onClick={this.handleFacebookClick}
          title="Facebook"
        />

        <Icon
          src={TwitterImage}
          title="Twitter"
          onClick={this.handleTwitterClick}
        />

        <Icon
          src={TumblrImage}
          onClick={this.handleTumblrClick}
          title="Tumblr"
        />
        <Icon
          src={TelegramImage}
          onClick={this.handleTelegramClick}
          title="Telegram"
        />
        <Icon
          src={GooglePlusImage}
          onClick={this.handleGooglePlusClick}
          title="Google Plus"
        />
        <Icon src={VkImage} onClick={this.handleVkClick} title="VKontakte" />
      </SocialsWrapper>
    )
  }
}

ShareOnSocial.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
}

export default ShareOnSocial

import { css } from 'styled-components'

const FullscreenButton = css`
  display: inline-flex;
  position: relative;
  border-radius: 2px;
  justify-content: center;
  align-items: center;
  background-color: #2196f3;
  color: #ffffff;
  box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2),
    0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12);
  cursor: pointer;
  border: none;
  outline: 0;
  user-select: none;
  text-align: center;
  text-decoration: none;

  &:hover {
    background-color: #006dc4;
  }
`

export default FullscreenButton

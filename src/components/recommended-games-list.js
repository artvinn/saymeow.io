import React from 'react'
import styled from 'styled-components'

import GameCard from './game-card'

const Wrapper = styled.div`
  display: grid;
  grid-template-rows: repeat(5, 1fr);
  margin-bottom: 5px;
  margin-left: 5px;
  margin-right: 5px;
  justify-content: space-between;
`

const RecommendedGamesList = ({ gameData }) => {
  return (
    <Wrapper>
      <GameCard {...{ gameData }} mini={true} />
      <GameCard {...{ gameData }} mini={true} />
      <GameCard {...{ gameData }} mini={true} />
      <GameCard {...{ gameData }} mini={true} />
      <GameCard {...{ gameData }} mini={true} />
    </Wrapper>
  )
}

export default RecommendedGamesList

import React from 'react'
import styled from 'styled-components'
import Link from 'gatsby-link'
import PropTypes from 'prop-types'

import media from '../utils/media-queries'

const Card = styled.div`
  display: grid;
  box-shadow: ${(props) =>
    props.mini ? 'none' : '0 4px 8px 0 rgba(0, 0, 0, 0.2);'}
  background: #ffffff;
  color: #000000;
  transition: all 100ms linear;
  margin: 0px;

  ${(props) =>
    props.mini
      ? ''
      : `
    &:hover {
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
      transform: scale(1.04);
      transition: all 100ms linear;

      p {
        display: block;
      }
    }
  `};
`

const TitleWrapper = styled.p`
  margin: 0;
  bottom: 0;
  line-height: 1.6em;
  text-align: center;
  font-weight: 700px;
  width: 100%;
  background-color: rgba(19, 131, 196, 0.9);
  color: #ffffff;
  position: absolute;
  display: none;
`

const Image = styled.img`
  width: 100%;
  border-radius: ${(props) => (props.mini ? '10px' : '0px')};
`

const GameCard = ({ gameKey, gameName, previewImageAltText, mini = false }) => (
  <Link
    to={`/games/${gameKey}`}
    style={{ textDecoration: 'none', margin: mini ? 7 : 0 }}
  >
    <Card mini={mini}>
      <Image
        mini={mini}
        src={`/images/games/${gameKey}.jpg`}
        //TODO: if mini add title
        alt={previewImageAltText}
      />
      {mini === false ? <TitleWrapper>{gameName}</TitleWrapper> : null}
    </Card>
  </Link>
)

GameCard.propTypes = {
  gameKey: PropTypes.string,
  gameName: PropTypes.string,
  previewImageAltText: PropTypes.string,
  mini: PropTypes.bool,
}

export default GameCard

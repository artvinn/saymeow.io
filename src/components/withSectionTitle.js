import React from 'react'

export default function withSectionTitle(WrappedComponent) {
  const hasTitle = (props) => (
    <div>
      <h2>{props.sectionName}</h2>
      <WrappedComponent {...props} />
    </div>
  )

  return hasTitle
}

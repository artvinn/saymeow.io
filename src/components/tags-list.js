import React from 'react'
import styled from 'styled-components'
import uuidv1 from 'uuid/v1'
import Link from 'gatsby-link'

import withSectionTitle from './withSectionTitle'

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  max-width: 1280px;
  font-size: 16px;
  margin-top: -15px;
`

const Tag = styled(Link)`
  background-color: #ffffff;
  border-radius: 5px;
  line-height: 40px;
  margin-top: 15px;
  margin-right: 20px;
  padding-right: 15px;
  padding-left: 15px;
  text-decoration: none;
  color: #000000;
  wdith: 100%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
`

const TagsList = ({ tags }) => (
  <Wrapper>
    {tags.map(({ name, key }) => (
      <Tag to={`/tags/${key}`} key={uuidv1()}>{`# ${name}`}</Tag>
    ))}
  </Wrapper>
)

export default withSectionTitle(TagsList)

import React, { Component } from 'react'
import styled from 'styled-components'

import GameIFrame from './game-iframe'
import GameControls from './game-controls'
import RecommendedGamesList from './recommended-games-list'

const Wrapper = styled.article`
  display: grid;
  grid-template-columns: ${(props) => props.gameWidth};
  grid-template-rows: auto 50px;
  box-shadow: 1px 1px 6px 0px rgba(0, 0, 0, 0.3);
  justify-self: center;
  background-color: #ffffff;
`

const normalFrameStyle = (width, height) => ({
  paddingBottom: height / width * 100 + '%',
  position: 'relative',
  height: 0,
  overflow: 'hidden',
  alignSelf: 'center',
})

const FullscreenFrameStyle = () => ({
  position: 'fixed',
  overflow: 'hidden',
  width: '100%',
  height: '100%',
  left: 0,
  top: 0,
  zIndex: 30,
  backgroundColor: '#000000',
})

class GameBox extends Component {
  constructor(props) {
    super()

    this.gameData = props.gameData

    this.state = {
      isFullscreen: false,
    }
  }

  enableFullscreen = () => {
    this.setState({ isFullscreen: true })
  }

  disableFullscreen = () => {
    this.setState({ isFullscreen: false })
  }

  render() {
    const { width, height } = this.gameData
    const isPortrait = height > width
    const gameColumnWidth = isPortrait
      ? 'minmax(200px, 400px)'
      : 'minmax(200px, 820px)'

    const frameStyle = this.state.isFullscreen
      ? FullscreenFrameStyle()
      : normalFrameStyle(width, height)

    return (
      <Wrapper gameWidth={gameColumnWidth}>
        {/* <RecommendedGamesList position="left" gameData={gameData} /> */}

        <div style={frameStyle}>
          <GameIFrame game={this.gameData} isMobile={false} />
        </div>

        <GameControls
          gameData={this.gameData}
          isFullscreen={this.state.isFullscreen}
          enableFullscreen={this.enableFullscreen}
          disableFullscreen={this.disableFullscreen}
        />

        {/* <RecommendedGamesList gameData={gameData} /> */}
      </Wrapper>
    )
  }
}

export default GameBox

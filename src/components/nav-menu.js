import React from 'react'
import styled from 'styled-components'
import uuidv1 from 'uuid/v1'
import Link from 'gatsby-link'

import kebabCase from '../utils/kebabCase'

const PAGES = [
  { name: 'Home', path: '/', title: 'saymeow.io - play best html5 games.' },
  ...[
    'Arcade',
    'Addicting',
    'Action',
    'Puzzle',
    'Mahjong',
    'Match 3',
    'Solitaire',
  ]
    .sort()
    .map((tag) => ({
      name: tag,
      path: `/tags/${kebabCase(tag)}`,
      title: `Play ${tag} games for free without download`,
    })),
]

const Wrapper = styled.nav`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: #2196f3;
  width: 100%;
  margin: 0 auto;
  height: 28px;
  padding: 0px;
  z-index: 3;
  border-top: 2px solid #1f82d1;
`

const Ul = styled.nav`
  display: inline-block;
  position: relative;
  list-style-type: none;

  li {
    padding: 0px 20px;
    display: inline;
  }
`

const StyledLink = styled(Link)`
  position: relative;
  color: #ffffff;
  text-decoration: none;

  &::after {
    content: '';
    background: white;
    mix-blend-mode: soft-light;
    width: calc(100% + 20px);
    position: absolute;
    bottom: -4px;
    left: -10px;
    transition: all 0.2s cubic-bezier(0.445, 0.05, 0.55, 0.95);
    height: ${({ selected }) => (selected ? 'calc(100% + 8px)' : 0)};
  }

  &:hover::after {
    height: calc(100% + 8px);
  }
`

const isOnPage = (path, location) => location.pathname === path

export default ({ location }) => (
  <Wrapper>
    <Ul style={{ listStyleType: 'none' }}>
      {PAGES.map(({ name, path, title }) => (
        <li key={uuidv1()}>
          <StyledLink
            selected={isOnPage(path, location)}
            title={title || ''}
            to={path}
          >
            {name}
          </StyledLink>
        </li>
      ))}
    </Ul>
  </Wrapper>
)

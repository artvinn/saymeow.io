import React, { Component } from 'react'
import styled from 'styled-components'

import ButtonStyle from './styles/button'
import Modal from './modal'
import ShareBox from './share-box'
import { ExpandIcon, ShareIcon, CrossIcon } from './ProjectIcons'

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
`

const Button = styled.button`
  ${ButtonStyle};

  div {
    padding: 4px;
  }
`

const ExitFullscreenButton = styled.button`
  ${ButtonStyle};

  position: fixed;
  top: 0;
  right: 0;
  padding: 7px;
  box-shadow: none;
  z-index: 31;

  div {
    padding-right: 5px;
  }

  &:hover {
    background-color: #2196f3;
  }
`

const GameName = styled.h1`
  margin-left: 10px;
`

class ControlsPanel extends Component {
  constructor() {
    super()

    this.state = {
      showShareBox: false,
    }
  }

  toggleShareBox = () => {
    this.setState({ showShareBox: !this.state.showShareBox })
  }

  render() {
    const {
      gameData,
      isFullscreen,
      enableFullscreen,
      disableFullscreen,
    } = this.props

    return (
      <Wrapper>
        <GameName>{gameData.name}</GameName>

        <div
          style={{
            display: 'inline-flex',
            width: 200,
            justifyContent: 'space-around',
          }}
        >
          <Button onClick={this.toggleShareBox}>
            <ShareIcon />
            <span>Share</span>
          </Button>

          <Button onClick={enableFullscreen}>
            <ExpandIcon />
            <span>Fullscreen</span>
          </Button>

          {this.state.showShareBox ? (
            <Modal onClose={this.toggleShareBox}>
              <ShareBox gameData={gameData} onClose={this.toggleShareBox} />
            </Modal>
          ) : null}
        </div>

        {isFullscreen ? (
          <ExitFullscreenButton onClick={disableFullscreen}>
            <CrossIcon size={16} />Exit Fullscreen
          </ExitFullscreenButton>
        ) : null}
      </Wrapper>
    )
  }
}

export default ControlsPanel

import { injectGlobal } from 'styled-components'
import fonts from '../fonts'

injectGlobal`
  html body {
    margin: 0;
    padding: 0;
    font-family: Roboto-Regular;
  }

  body {
    background-color: #DCEBF7;
  }

  @font-face {
    font-family: "Roboto-Regular";
    font-style: normal;
    font-weight: normal;
    src: url('${fonts.RobotoRegularTTF}');
  }
`

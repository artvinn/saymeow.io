import { css } from 'styled-components'

const sizes = {
  desktop: { min: 1281 },
  laptop: { min: 1025, max: 1280 },
  tablet: { min: 768, max: 1024 },
  mobile: { min: 481, max: 767 },
  mobileSmall: { max: 480 },
}

// iterate through the sizes and create a media template
const media = Object.keys(sizes).reduce((accumulator, label) => {
  // use em in breakpoints to work properly cross-browser and support users
  // changing their browsers font-size: https://zellwk.com/blog/media-query-units/
  const emMinSize = sizes[label].min / 16
  const emMaxSize = sizes[label].max / 16
  const min = (...args) => css`
    @media (min-width: ${emMinSize}em) {
      ${css(...args)};
    }
  `
  const max = (...args) => css`
    @media (max-width: ${emMaxSize}em) {
      ${css(...args)};
    }
  `

  const minMax = (...args) => css`
    @media (min-width: ${emMinSize}em) and (max-width: ${emMaxSize}em) {
      ${css(...args)};
    }
  `

  accumulator[label] =
    Object.keys(sizes[label]).length === 2
      ? minMax
      : sizes[label].min
        ? min
        : max
  // accumulator[label] = (...args) => css`
  //   @media (max-width: ${emSize}em) {
  //     ${css(...args)};
  //   }
  // `
  return accumulator
}, {})

export default media

const crypto = require(`crypto`)
const axios = require('axios')
const _ = require('lodash')

const API_URL = 'https://pure-forest-76911.herokuapp.com/api'

exports.sourceNodes = async ({ boundActionCreators }) => {
  // if (process.env.NODE_ENV !== 'production') return

  const { createNode } = boundActionCreators

  // fetch games list from database
  let res = await axios.get(`${API_URL}/v1/games`)

  const data = JSON.parse(JSON.stringify(res.data))
  const tags = _.uniq(_.flatten(data.map((el) => el.tags)))

  // create tag nodes
  tags.forEach((tag) => {
    const tagNode = {
      name: tag,
      key: _.kebabCase(tag),
      numOfGames: data.filter((game) => game.tags.includes(tag)).length,
      id: `tag-${_.kebabCase(tag)}`,
      parent: null,
      children: [],
      internal: {
        type: 'Tags',
        mediaType: 'text',
      },
    }

    const contentDigest = crypto
      .createHash('md5')
      .update(JSON.stringify(tagNode))
      .digest('hex')

    tagNode.internal.contentDigest = contentDigest
    createNode(tagNode)
  })

  // create game nodes
  data.forEach((el) => {
    const gameNode = {
      ...el,
      previewImageAltText: el.metaDescription + 'Play it now at saymeow.io!', //TODO: localize. trim spaces and add space befor play
      parent: null,
      id: `game-${el.key}`,
      children: [],
      internal: {
        type: 'GamesJson',
        mediaType: 'application/json',
      },
    }

    const contentDigest = crypto
      .createHash('md5')
      .update(JSON.stringify(gameNode))
      .digest('hex')

    gameNode.internal.contentDigest = contentDigest
    createNode(gameNode)
  })

  return
}

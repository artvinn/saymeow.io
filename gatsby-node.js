const path = require('path')
const _ = require('lodash')

exports.createPages = ({ boundActionCreators, graphql }) => {
  const { createPage } = boundActionCreators

  return new Promise((resolve, reject) => {
    const gamePageTemplate = path.resolve('src/templates/game.js')
    const gamePlayPageTemplate = path.resolve('src/templates/game-play.js')
    const tagPageTemplate = path.resolve('src/templates/tag.js')
    resolve(
      graphql(
        `
          {
            games: allGamesJson {
              edges {
                node {
                  name
                  key
                  description
                  metaDescription
                  tags
                }
              }
            }
          }
        `
      ).then((result) => {
        if (result.errors) {
          reject(result.errors)
        }

        const games = result.data.games.edges

        // create game pages
        games.forEach(({ node }) => {
          createPage({
            path: `/games/${node.key}`,
            component: gamePageTemplate,
            context: { key: node.key },
          })
        })

        // create /play game pages for mobile devices(it only loads game in iframe)
        games.forEach(({ node }) => {
          createPage({
            path: `/games/${node.key}/play`,
            component: gamePlayPageTemplate,
            context: { key: node.key },
            layout: 'game-play',
          })
        })

        let tags = []
        _.each(games, (edge) => {
          if (_.get(edge, 'node.tags')) {
            tags = tags.concat(edge.node.tags)
          }
        })

        tags = _.uniq(tags)

        // create tag pages
        tags.forEach((tag) => {
          createPage({
            path: `/tags/${_.kebabCase(tag)}`,
            component: tagPageTemplate,
            context: {
              tag,
            },
          })
        })
      })
    )
  })
}

module.exports = {
  pathPrefix: '/',
  siteMetadata: {
    title: `Saymeow`,
    siteUrl: `https://www.saymeow.io`,
    previewImageSize: 200,
  },
  plugins: [
    'gatsby-source-db',
    'gatsby-plugin-styled-components',
    'gatsby-plugin-react-helmet',
    {
      resolve: '@andrew-codes/gatsby-plugin-elasticlunr-search',
      options: {
        fields: ['name', 'key'],
        resolvers: {
          GamesJson: {
            name: (node) => node.name,
            key: (node) => node.key,
            previewImageAltText: (node) => node.previewImageAltText,
            createdAt: (node) => node.createdAt,
          },
          Tags: {
            name: (node) => node.name,
            key: (node) => node.key,
            numOfGames: (node) => node.numOfGames,
          },
        },
      },
    },
    {
      resolve: 'gatsby-plugin-google-analytics',
      options: {
        trackingId: 'UA-118961266-1',
        head: true,
        anonymize: true,
      },
    },
    {
      resolve: 'gatsby-plugin-sitemap',
      options: {
        query: `
        {
          site {
            siteMetadata {
              siteUrl
            }
          }

          allSitePage {
            edges {
              node {
                path
              }
            }
          }
        }
      `,
      },
    },
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: 'https://saymeow.io',
        sitemap: 'https://saymeow/sitemap.xml',
        policy: [{ userAgent: '*', allow: '/' }],
      },
    },
  ],
}
